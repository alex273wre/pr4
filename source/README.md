# Перевод даты из формата _DD/MM/YYYY_ на естественный язык.

__Проверка ввода.__

Проверка ввода происходит в функции _enter()_.

```
def enter(date):
    if len(date) != 10:
        raise ValueError()
    # Паттерн регулярного выражения
    template = re.compile(r'\d\d\/\d\d\/\d\d\d\d')
    res = re.search(template, date)
    if res == None:
        raise TypeError()
    res = res.group()
    return res

```
Входные данные:

* строка в формате _DD/MM/YYYY_.

Возможные ошибки:

* длина ввода больше или меньше 10 символов.
* _TypeError_, если введены любые символы кроме цифр и символ - /.

__Перевод цифр в слова.__

Конвертация введенной строки даты на естественный язык.

Конвертирование происходит в функции _nums_to_words()._

Так разбивается строка на значение даты(день):
```
for i in range(2):
        day += res[i]
    day = int(day)
```

Затем происходит проверка на корректность чисел, например, чтобы не было больше
13 месяцев, или 29 дней в феврале в не високосный год.

![alt gtf](https://sun9-9.userapi.com/c855736/v855736215/1af77d/hxDpsA66cvc.jpg)

Проверка февраля в високосный день:
```
elif year % 4 == 0:
    if month == 2 and day > 29:
        return 'В високосном году, в феврале 29 дней, попробуйте заново'
```

Нахождение соответствий числа со словом в словаре:
```
for key in before_2000_middle.keys():
    if middle_year_19 == key:
        year_word = f'nineteen {before_2000_middle[key]}'
    for key in before_2000_end.keys():
        if end_year_19 == key:
            year_word += before_2000_end[key]
```

Пример словаря
```
before_2000_middle = {7:'seventy', 8:'eighty', 9:'ninety'}
```
