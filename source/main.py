"""
Минченко Алексей Александрович
КИ19-17/1б
Дана дата в формате DD/MM в которой указаны только день и месяц.
Необходимо представить дату в формате «day of Month»
(иначе говоря, записать дату на естественном языке).
Примечание: в дате не указан год, поэтому для выполнения задачи укажем,
что год для введённой даты всегда равен 2020-ому.
"""
import re

day_of_words = ['first','second','third',
                'fourth', 'fifth', 'sixth',
                'seventh', 'eighth', 'ninth',
                'tenth', 'eleventh', 'twelfth',
                'thirteenth', 'fourteenth', 'fifteenth',
                'sixteenth', 'seventeenth', 'eighteenth',
                'nineteenth', 'twentieth', 'twentieth first',
                'twentieth second', 'twentieth third',
                'twentieth fourth', 'twentieth fifth',
                'twentieth sixth', 'twentieth seventh',
                'twentieth eighth', 'twentieth ninth', 'thirtieth',
                'thirtieth first']
month_of_words = ['January', 'February', 'March',
                  'April', 'May', 'June',
                  'July', 'August', 'September',
                  'October', 'November', 'December']
before_2000_middle = {7:'seventy', 8:'eighty', 9:'ninety'}
before_2000_end = {0:'', 1:'-one',2:'-two',3:'-three',
                   4:'-four', 5:'-five', 6:'-six',
                   7:'-seventh', 8:'-eight', 9:'-nine'}

after_2000 = {0:'zero', 1:'one', 2:'two', 3:'three',
              4:'four', 5:'five', 6:'six',
              7:'seven', 8:'eight', 9:'nine',
              10:'ten', 11:'eleven', 12:'twelve',
              13:'thirteen', 14:'fourteen', 15:'fifteen',
              16:'sixteen', 17:'seventeen', 18:'eighteen',
              19:'nineteen', 20:'twenty'}

## Функция для проверки ввода.
#
#  Принимает на вход дату через слэши.
#  Проверяет строку на длину и на тип введенных данных.
#
#  @param date Строка введенная пользователем.
#
#  @return Корректная строка.
#
#  @throw TypeError
#  @throw ValueError.
#        
#
#  @b Examples:
#  @code{.py}
#        >>> enter('23/04/2001')
#        '23/04/2001'
#        >>> enter('string')
#        Traceback (most recent call last):
#            ...
#            raise ValueError()
#        ValueError
#        >>> enter('23/04/20001')
#        Traceback (most recent call last):
#            ...
#            raise ValueError()
#        ValueError
#  @endcode
def enter(date):
    if len(date) != 10:
        raise ValueError()
    # Паттерн регулярного выражения
    template = re.compile(r'\d\d\/\d\d\/\d\d\d\d')
    res = re.search(template, date)
    if res == None:
        raise TypeError()
    res = res.group()
    return res

## Функция переводящая строку из цифр в слова.
#
#  Принимает на вход один аргумент - корректную строку даты.
#  Эта строка разбивается на переменные - день, месяц, год.
#  Затем происходит проверка этих переменных на соответствие
#  с правильными значениями даты. После происходит сравнение
#  значений переменных(день, месяц, год) с соответствующим
#  им словестным значениям в словаре и вывод даты на естественном
#  языке 
#  
#  @param res - Корректная строка.
#    
#  @return Дата на естественном языке.  
#  
#  @throw Нет.
#
#
#  @b 
#  Examples
#  @code{.py}
#  >>> nums_to_words('23/04/2001')
#  'twentieth third April twenty zero one'
#  @endcode
def nums_to_words(res):
    day = ''
    month = ''
    year = ''
    for i in range(2):
        day += res[i]
    day = int(day)
    for i in range(3, 5):
        month += res[i]
    month = int(month)
    for i in range(6, 10):
        year += res[i]
    year = int(year)
    if ((1970 <= year <= 2020) and (1 <= day <= 31) and
        (1 <= month <= 12)):
        if month == 4 and day > 30:
            return 'В апреле всего 30 дней, попробуйте заново'
        elif month == 6 and day > 30:
            return 'В июне всего 30 дней, попробуйте заново'
        elif month == 9 and day > 30:
            return 'В сентябре всего 30 дней, попробуйте заново'
        elif month == 11 and day > 30:
                return 'В ноябре всего 30 дней, попробуйте заново'
        elif year % 4 == 0:
            if month == 2 and day > 29:
                return 'В високосном году, в феврале 29 дней, попробуйте заново'
        elif month == 2 and day > 28:
                return 'В феврале всего 28 дней, попробуйте заново'
    else:
        return 'Введена не существующая дата, попробуйте заново'
    start_year = year//100
    end_year_19 = year % 10
    middle_year_19 = (year // 10) % 10
    end_year_20 = year % 100
    for i in range(31):
        if i == day - 1:
            day_word = day_of_words[i]
            continue
    for g in range(12):
        if g == month - 1:
            month_word = month_of_words[g]
            continue
    if start_year == 20:
        if end_year_20 < 10:
            for key in after_2000.keys():
                if end_year_20 == key:
                    year_word = f'twenty zero {after_2000[key]}'
        else:
            for key in after_2000.keys():
                if end_year_20 == key:
                    year_word = f'twenty {after_2000[key]}'
    else:
        for key in before_2000_middle.keys():
            if middle_year_19 == key:
                year_word = f'nineteen {before_2000_middle[key]}'
        for key in before_2000_end.keys():
            if end_year_19 == key:
                year_word += before_2000_end[key]
    back = f'{day_word} {month_word} {year_word}'
    return back

## Главная функция, выполняется при запуске.
def main():
    print(nums_to_words(enter(input('Введите дату в формате ДД/ММ/ГГГГ: '))))



if __name__ == '__main__':
    main()
