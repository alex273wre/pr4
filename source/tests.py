from main import enter
import pytest


## Проверка на некорректный ввод
def test_words_enter():
    with pytest.raises(ValueError):
        enter("afdgs")


## Неправильная длина ввода (меньше 10)
def test_len_enter_less():
    with pytest.raises(ValueError):
        enter("12/12/201")


## Неправильная длина ввода (больше 10)
def test_len_enter_more():
    with pytest.raises(ValueError):
        enter("23/04/20001")


## Корректный ввод
def test_right():
    assert enter('23/04/2001') == '23/04/2001'
