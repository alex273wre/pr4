from main import nums_to_words
import pytest


## Случай без ошибок
def test_right():
    assert (nums_to_words('23/04/1999') ==
            'twentieth third April nineteen ninety-nine')


## Неправильный ввод дня
def test_fail_day():
    assert (nums_to_words('32/12/1999') ==
            'Введена не существующая дата, попробуйте заново')


## Неправильный ввод месяца
def test_fail_month():
    assert (nums_to_words('23/14/1999') ==
            'Введена не существующая дата, попробуйте заново')


## Неправильный ввод года
def test_fail_year():
    assert (nums_to_words('23/04/1955') ==
            'Введена не существующая дата, попробуйте заново')


## Проверка на високосный год
def test_vesocos():
    assert (nums_to_words('30/02/2000') ==
            'В високосном году, в феврале 29 дней, попробуйте заново')


## Непрвильный ввод февраля
def test_feb():
    assert (nums_to_words('29/02/2001') ==
            'В феврале всего 28 дней, попробуйте заново')


## Правильный ввод февраля
def test_right_feb():
    assert (nums_to_words('29/02/2004') ==
            'twentieth ninth February twenty zero four')


## Проверка дней в апреле
def test_april():
    assert (nums_to_words('31/04/2001') ==
            'В апреле всего 30 дней, попробуйте заново')


## Проверка дней в июне
def test_june():
    assert (nums_to_words('31/06/2004') ==
            'В июне всего 30 дней, попробуйте заново')


## Проверка дней в сентябре
def test_sept():
    assert (nums_to_words('31/09/1986') ==
            'В сентябре всего 30 дней, попробуйте заново')


## Проверка дней в ноябре
def test_nov():
    assert (nums_to_words('31/11/2011') ==
           'В ноябре всего 30 дней, попробуйте заново')
